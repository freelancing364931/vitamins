from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist

from .models import (
    Bot,
    Configurations,
    Deploy,
    Files
    
    
    )



class ConfigurationsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Configurations
        fields = '__all__'
    
class ConfigurationsInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Configurations
        depth = 1
        fields = '__all__' 
        
class DeploySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Deploy
        fields = '__all__'
        
class DeployInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deploy
        depth = 1
        fields = '__all__'
            
class FilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        fields = '__all__'
        
    def create(self, validated_data):
        request = self.context.get('request')
        if request and 'file' in request.data:
            validated_data['file_name'] = request.data['file'].name
        return super().create(validated_data)

    def update(self, instance, validated_data):
        request = self.context.get('request')
        if request and 'file' in request.data:
            validated_data['file_name'] = request.data['file'].name
        return super().update(instance, validated_data)

        
class FilesInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        depth = 1
        fields = '__all__'

class BotSerializer(serializers.ModelSerializer):
    configurations = ConfigurationsInfoSerializer(many=True, read_only=True)
    deployement = DeployInfoSerializer(many=True, read_only=True)
    files = FilesInfoSerializer(many=True, read_only=True)
    class Meta:
        model = Bot
        fields = '__all__'
        
class BotInfoSerializer(serializers.ModelSerializer):
    configurations = ConfigurationsInfoSerializer(many=True, read_only=True)
    deployement = DeployInfoSerializer(many=True, read_only=True)
    files = FilesInfoSerializer(many=True, read_only=True)

    class Meta:
        model = Bot
        depth = 1
        fields = '__all__'
        