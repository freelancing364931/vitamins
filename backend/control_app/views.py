from django.shortcuts import render
# from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework import status, generics,permissions, viewsets, parsers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_simplejwt.tokens import RefreshToken
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from django.http import JsonResponse
from rest_framework_simplejwt.authentication import JWTAuthentication

from rest_framework.exceptions import NotFound
import jwt
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.urls import reverse
from django.shortcuts import render
import random as rand
from django.conf import settings
from Utils.utils import Util

from .models import (
    Bot,
    Deploy,
    Configurations,
    Files
)

from .serializers  import (
    BotSerializer,
    BotInfoSerializer,
    DeploySerializer,
    DeployInfoSerializer,
    ConfigurationsSerializer,
    ConfigurationsInfoSerializer,
    FilesInfoSerializer,
    FilesSerializer
)

from django.shortcuts import redirect
import datetime
    
    

class BotViewSet(viewsets.ModelViewSet):
    queryset = Bot.objects.filter(is_active=True)
    permission_classes = (permissions.IsAuthenticated,)
    
    filterset_fields = ['title',
                        'description',
                        'id',
                        'is_active',
                        'created_at',
                        'updated_at']

    def get_serializer_class(self):
        if self.request is None:
            return BotSerializer
        elif not self.request.method in permissions.SAFE_METHODS:
            return BotSerializer
        return BotInfoSerializer



class ConfigurationsViewSet(viewsets.ModelViewSet):
    queryset = Configurations.objects.filter(is_active=True)
    permission_classes = (permissions.IsAuthenticated,)    
    
    filterset_fields = ['temperature',
                        'bot',
                        'title',
                        'id',
                        'open_ai_key_api',
                        'model_name',
                        'max_token',
                        'welcome_message',
                        'color',
                        'open_ai_key_config',
                        'is_active',
                        'created_at',
                        'updated_at']

    def get_serializer_class(self):
        if self.request is None:
            return ConfigurationsSerializer
        elif not self.request.method in permissions.SAFE_METHODS:
            return ConfigurationsSerializer
        return ConfigurationsInfoSerializer



class DeployViewSet(viewsets.ModelViewSet):
    queryset = Deploy.objects.filter(is_active=True)
    permission_classes = (permissions.IsAuthenticated,)
    
    filterset_fields = ['varification_token',
                        'id',
                        'bot',
                        'access_token',
                        'phone_number',
                        'web_hook_url',
                        'header',
                        'base_url',
                        'is_active',
                        'created_at',
                        'updated_at']

    def get_serializer_class(self):
        if self.request is None:
            return DeploySerializer
        elif not self.request.method in permissions.SAFE_METHODS:
            return DeploySerializer
        return DeployInfoSerializer


class FilesViewSet(viewsets.ModelViewSet):
    queryset = Files.objects.filter(is_active=True)
    permission_classes = (permissions.IsAuthenticated,)
    
    filterset_fields = ['id',
                        'bot',
                        'is_active',
                        'created_at',
                        'updated_at']
    
    parser_classes = [parsers.MultiPartParser]

    def get_serializer_class(self):
        if self.request is None:
            return FilesSerializer
        elif not self.request.method in permissions.SAFE_METHODS:
            return FilesSerializer
        return FilesInfoSerializer
