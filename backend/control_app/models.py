from pyexpat import model
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionManager, BaseUserManager, PermissionsMixin, UserManager
)
import uuid
# Create your models here.



class Bot(models.Model):
    
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Bot'
        verbose_name_plural = 'Bots'
        ordering = ['-created_at']
        

class Configurations(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bot = models.ForeignKey(Bot, on_delete=models.DO_NOTHING, related_name="configurations")
    title = models.CharField(max_length=255)

    open_ai_key_api = models.CharField(max_length=255)
    temperature = models.CharField(max_length=255, null=True, blank=True)
    model_name = models.CharField(max_length=255, null=True, blank=True)
    max_token = models.CharField(max_length=255, null=True, blank=True)
    
    welcome_message = models.TextField(max_length=255, blank=True, null=True)
    color = models.CharField(max_length=255, null=True, blank=True)
    logo = models.ImageField(upload_to="static/logo", blank=True, null=True)
    open_ai_key_config = models.CharField(max_length=255, null=True)
    is_active = models.BooleanField(default=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Configuration'
        verbose_name_plural = 'Configurations'
        ordering = ['-created_at']
        


class Deploy(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bot = models.ForeignKey(Bot, on_delete=models.DO_NOTHING, related_name="deployement")
    varification_token = models.TextField(max_length=255, null=True, blank=True)
    access_token = models.TextField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    
    web_hook_url = models.CharField(max_length=255, null=True, blank=True)
    header = models.CharField(max_length=255, null=True, blank=True)
    base_url = models.CharField(max_length=255, null=True, blank=True)
    
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    

    def __str__(self):
        return self.access_token
    
    class Meta:
        verbose_name = 'Deploy'
        verbose_name_plural = 'Deployments'
        ordering = ['-created_at']
        
        
class Files(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bot = models.ForeignKey(Bot, on_delete=models.DO_NOTHING, related_name="files")
    file = models.FileField(upload_to="static/files")    
    file_name = models.CharField(max_length= 255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    

    def __str__(self):
        return self.access_token
    
    class Meta:
        verbose_name = 'Deploy'
        verbose_name_plural = 'Deployments'
        ordering = ['-created_at']
        