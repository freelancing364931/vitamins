from django.contrib import admin
from control_app import models
# Register your models here.
admin.site.register((models.Configurations, models.Bot, models.Deploy))