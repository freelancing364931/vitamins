# Generated by Django 4.2 on 2023-08-03 09:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("control_app", "0003_alter_files_bot"),
    ]

    operations = [
        migrations.AddField(
            model_name="files",
            name="file_name",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
