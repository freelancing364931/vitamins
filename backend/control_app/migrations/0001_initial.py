# Generated by Django 4.2 on 2023-07-30 14:12

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Bot",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("title", models.CharField(max_length=255)),
                ("description", models.TextField()),
                ("is_active", models.BooleanField(default=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
            ],
            options={
                "verbose_name": "Bot",
                "verbose_name_plural": "Bots",
                "ordering": ["-created_at"],
            },
        ),
        migrations.CreateModel(
            name="Deploy",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "varification_token",
                    models.TextField(blank=True, max_length=255, null=True),
                ),
                (
                    "access_token",
                    models.TextField(blank=True, max_length=255, null=True),
                ),
                (
                    "phone_number",
                    models.CharField(blank=True, max_length=255, null=True),
                ),
                (
                    "web_hook_url",
                    models.CharField(blank=True, max_length=255, null=True),
                ),
                ("header", models.CharField(blank=True, max_length=255, null=True)),
                ("base_url", models.CharField(blank=True, max_length=255, null=True)),
                ("is_active", models.BooleanField(default=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "bot",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="control_app.bot",
                    ),
                ),
            ],
            options={
                "verbose_name": "Deploy",
                "verbose_name_plural": "Deployments",
                "ordering": ["-created_at"],
            },
        ),
        migrations.CreateModel(
            name="Configurations",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("title", models.CharField(max_length=255)),
                ("open_ai_key_api", models.CharField(max_length=255)),
                (
                    "temperature",
                    models.CharField(blank=True, max_length=255, null=True),
                ),
                ("model_name", models.CharField(blank=True, max_length=255, null=True)),
                ("max_token", models.CharField(blank=True, max_length=255, null=True)),
                (
                    "welcome_message",
                    models.TextField(blank=True, max_length=255, null=True),
                ),
                ("color", models.CharField(blank=True, max_length=255, null=True)),
                (
                    "logo",
                    models.ImageField(blank=True, null=True, upload_to="static/logo"),
                ),
                ("open_ai_key_config", models.CharField(max_length=255, null=True)),
                ("is_active", models.BooleanField(default=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "bot",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="control_app.bot",
                    ),
                ),
            ],
            options={
                "verbose_name": "Configuration",
                "verbose_name_plural": "Configurations",
                "ordering": ["-created_at"],
            },
        ),
    ]
