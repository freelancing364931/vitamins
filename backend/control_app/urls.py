from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from control_app import views


router = DefaultRouter()


router.register("bot", views.BotViewSet, "bot")
router.register("configuration", views.ConfigurationsViewSet, "configuration")
router.register("deploy", views.DeployViewSet, "deploy")
router.register("files", views.FilesViewSet, "files")
# router.register("bot/", views.BotViewSet, "Bot ")

urlpatterns = [
    path("", include(router.urls)),
]
