from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from accounts.views import (
    RegisterViewSet,
    TokenObtainPairViewSet,
    TokenRefreshViewSet,
    EmailVarification,
    VerifyEmailView,
    LoginViewset
)
from rest_framework_simplejwt.views import TokenObtainPairView, TokenVerifyView, TokenRefreshView

# from rest_framework_simplejwt.views import (
#     TokenObtainPairView,
#     TokenRefreshView,
# )


router = DefaultRouter()


router.register("register", RegisterViewSet, "register")
router.register("token", TokenObtainPairViewSet, "token")
router.register("token-refresh", TokenRefreshViewSet, "token-refresh")
router.register("login", LoginViewset, "login")
# router.register("email-verify", EmailVerify, "Email Verify")

urlpatterns = [
    path("", include(router.urls)),
    path("email", EmailVarification, name="email_send"),
    path('email_verify/', VerifyEmailView.as_view(), name='email_verify'),
    # path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
