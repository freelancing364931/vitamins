from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from rest_framework import serializers
from accounts.models import CustomUser
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.renderers import (
    HTMLFormRenderer,
    JSONRenderer,
    BrowsableAPIRenderer,
)

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    Custom TokenObtainPairSerializer that includes additional user data in the response.
    """
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        # # Add custom claims to the token payload
        # token['email'] = user.email
        # token['first_name'] = user.first_name
        # token['last_name'] = user.last_name
        return token

class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField()
    # fullname = serializers.CharField()
    password = serializers.CharField()
    confirm_password = serializers.CharField()

class CustomUserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = CustomUser
        fields = "__all__"
        exclude = ("password",)


class VerifyEmailSerializer(serializers.Serializer):
    renderer_classes = (BrowsableAPIRenderer, JSONRenderer, HTMLFormRenderer)
    email = serializers.EmailField(min_length=2)

    class Meta:
        fields = ('email',)

class TokenObtainSerilizer(serializers.Serializer):
    
    email = serializers.EmailField()
    password = serializers.CharField()
    
class TokenRefreshSerializer(serializers.Serializer):
    token = serializers.CharField()
    
