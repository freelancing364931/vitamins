from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework import status, generics,permissions
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_simplejwt.tokens import RefreshToken
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from django.http import JsonResponse
from rest_framework_simplejwt.authentication import JWTAuthentication

from rest_framework.exceptions import NotFound
import jwt
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.urls import reverse
from django.shortcuts import render
import random as rand
from django.conf import settings
from Utils.utils import Util

from accounts.models import (
    CustomUser,
)
from accounts.serializers import(
    RegistrationSerializer,
    TokenObtainSerilizer,
    CustomUserSerializer,
    TokenRefreshSerializer,
    VerifyEmailSerializer,
    CustomTokenObtainPairSerializer,
)

from django.shortcuts import redirect
import datetime
    
def getUser(user):
    return {
        "fullname": str(user.fullname),
        "role": str(user.role),
        "user": str(user.email),
        
    }

class RegisterViewSet(ModelViewSet):
    serializer_class = RegistrationSerializer
    http_method_names = ['post']
    queryset = CustomUser.objects.all()
    # permission_classes = (IsAuthenticatedCustom, )
    def create(self, request):
        valid_request = self.serializer_class(data = request.data)
        valid_request.is_valid(raise_exception=True)
        user = CustomUser.objects.filter(email = valid_request.validated_data["email"])
        if not user:
            if valid_request.validated_data["password"] == valid_request.validated_data['confirm_password']:
                user = CustomUser.objects.create(
                    email= valid_request.validated_data['email'],
                )
                user.set_password(valid_request.validated_data['password'])
                # user.save()
                try:
                    token = AccessToken().for_user(user)
                    token.set_exp(lifetime=datetime.timedelta(minutes=10))
                    current_site = request.META['HTTP_HOST']
                    reletive_link = reverse('email_verify')
                    absurl = 'http://' + current_site + reletive_link + "?token=" + str(token)
                    html_message = render_to_string('pages/Email_varification.html', {'address': absurl})
                    plain_message = strip_tags(html_message)
                    from_email = 'From <{}>'.format(settings.EMAIL_HOST_USER)
                    body = "Hi " + request.data['email'] + " welcome go to the link below to activate your account \n" + absurl
                    to = "{}".format(settings.EMAIL_HOST_USER)
                    data = {"subject": 'Email Verification', "body": body, "to": [to], "from": from_email,
                            "plain_message": plain_message, "html_message": html_message}
                    Util.send_email(data)
                    return Response({"success": "User Created Successfully"}, status=status.HTTP_200_OK)
                except:
                    raise Exception("Error")        
                
            return Response({"error": "Password Not matched"}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"error":"User already Exist"}, status=status.HTTP_400_BAD_REQUEST)


class TokenObtainPairViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    http_method_names = ['post']
    serializer_class = TokenObtainSerilizer
    
    def create(self, request):
        valid_serializer = self.serializer_class(data = request.data)
        valid_serializer.is_valid(raise_exception=True)
        
        try:
            user = CustomUser.objects.get(email = valid_serializer.validated_data["email"])
        except ObjectDoesNotExist:
            return Response({
                "error": "User Does not Exist"
            }, status=status.HTTP_400_BAD_REQUEST)
            
        auth = user.check_password(valid_serializer.validated_data["password"])
        if auth:
            try:
                token = AccessToken().for_user(user)
                refresh = RefreshToken().for_user(user)
                return Response({
                    "access": str(token),
                    "refresh": str(refresh),
                    "user": getUser(user)
                })
            except:
                return Response({
                    "error": "Can't Create Token"
                }, status=status.HTTP_404_NOT_FOUND)
        return Response({
            "error": "Please Enter a Valid Email or Password"
        }, status=status.HTTP_400_BAD_REQUEST)
        
        
        
class TokenRefreshViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    http_method_names = ['post']
    serializer_class = TokenRefreshSerializer
    
    def create(self, request):
        valid_serializer = self.serializer_class(data = request.data)
        valid_serializer.is_valid(raise_exception=True)
        
        try:
            decoded_payload = JWTAuthentication().get_validated_token(valid_serializer.validated_data["token"])
            user_id = decoded_payload['user_id']
            user = CustomUser.objects.get(id=user_id)
            access = AccessToken().for_user(user)
            refresh = RefreshToken().for_user(user)
            return Response({
                "access": str(access),
                "refresh": str(refresh),
                # "user": getUser(user)
            })
        except:
            return Response({
                "error": "Unauthorized"
            }, status=status.HTTP_401_UNAUTHORIZED)
            

class VerifyEmailView(generics.GenericAPIView):
    serializer_class = VerifyEmailSerializer

    def get(self, request):
        token = request.GET.get('token')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, "HS256")
            user = CustomUser.objects.get(id=payload['user_id'])
            user.is_verified = True
            user.save()
            # return render(request, 'mail-welcome.html', {"email": user.email})
            return redirect(settings.REACT_APP_URL)
        except:
            raise NotFound('validation error')

            
            
                       
def EmailVarification(request):
    return render(
        request, "pages/Email_varification.html"
    )           
            
        
class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

class LoginViewset(ModelViewSet):
    permission_classes = (permissions.AllowAny,)
    queryset = CustomUser.objects.all()
    serializer_class = CustomTokenObtainPairSerializer

    def create(self, request, *args, **kwargs):
        # Invoke CustomTokenObtainPairView directly using as_view()
        response = CustomTokenObtainPairView.as_view()(request._request)
        return Response(response.data, status=status.HTTP_200_OK)

    