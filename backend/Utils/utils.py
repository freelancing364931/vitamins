import jwt
from datetime import datetime, timedelta
from django.conf import settings
from accounts.models import CustomUser
from rest_framework.pagination import PageNumberPagination
import re 
from django.db.models import Q 
import datetime
from django.template.loader import TemplateDoesNotExist, render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail, EmailMultiAlternatives, EmailMessage
from django.contrib.staticfiles.storage import staticfiles_storage
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from django.urls import reverse


def get_access_token(payload, minute):
    token = jwt.encode({'exp': datetime.now() + timedelta(minutes=minute), **payload}, settings.SECRET_KEY, algorithm="HS256")

    return token

def decodeJWT(bearer):
    if not bearer:
        return None
    
    token = bearer[7: ]

    try: 
        decoded = jwt.decode(
            token, key=settings.SECRET_KEY, algorithms="HS256"
        )
        
    except Exception:
        return None
    
    if decoded: 
        try: 
            return CustomUser.objects.get(id = decoded['user_id'])
        except Exception:
            return None

class Util():
    @staticmethod
    def send_email(data):
        send_mail(data["subject"], data["plain_message"], data["from"], data["to"],
                       html_message=data["html_message"])
# import jwt
# from datetime import datetime, timedelta
# from django.conf import settings
# from accounts.models import CustomUser
# from rest_framework.pagination import PageNumberPagination
# import re 
# from django.db.models import Q 
# import datetime
# from django.template.loader import TemplateDoesNotExist, render_to_string
# from django.utils.html import strip_tags
# from django.core import mail

# def get_access_token(payload, minute):
#     token = jwt.encode({'exp': datetime.now() + timedelta(minutes=minute), **payload}, settings.SECRET_KEY, algorithm="HS256")

#     return token

# def decodeJWT(bearer):
#     if not bearer:
#         return None
    
#     token = bearer[7: ]

#     try: 
#         decoded = jwt.decode(
#             token, key=settings.SECRET_KEY, algorithms="HS256"
#         )
        
#     except Exception:
#         return None
    
#     if decoded: 
#         try: 
#             return CustomUser.objects.get(id = decoded['user_id'])
#         except Exception:
#             return None

# def send_verification_email(request, user):
    
#     # current_site = request.META['HTTP_HOST']    
    
#     token = AccessToken().for_user(user)
#     current_site = request.META['HTTP_HOST']
#     reletive_link = reverse('email_verify')
#     absurl = 'http://' + current_site + reletive_link + "?token=" + str(token)
#     html_message = render_to_string('pages/Email_varification.html', {'address': absurl})
#     plain_message = strip_tags(html_message)
    
#     # html_message = render_to_string('pages/Email_varification.html', {'address': "absurl"})
#     # plain_message = strip_tags(html_message)

#     email = EmailMultiAlternatives(
#         subject='Email subject',
#         body=strip_tags(plain_message),
#         from_email='coder.space1007@gmail.com',
#         to=['coder.space1007@gmail.com'],
#     )
    
#     email.attach_alternative(html_message, 'text/html')
#     email.send()
    
