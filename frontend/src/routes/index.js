// import logo from './logo.svg';
// import { Route, Router, useLocation, Navigate, Routes } from 'react-router-dom';
import React, {useState, useEffect} from 'react';

// import AuthRouter from './auth/Authroute';


import { useSelector } from 'react-redux';
import Auth from './auth/Auth';
import Casual from './casual/Casual';
import Layout from '../pages/Dashboard/Layout/Layout';
import { Route, Routes } from "react-router-dom"

import { 
  Confirmation, 
  Login, 
  Registration,
  ForgetPassword,
  CreateNewFile,
  CreateNewFileText,
  Configuration,
  Deploy,
  Logs,
  Home
} from "../pages"



function App() {
  // const {authenticate, setAuthenticate} = useState(true);
  const credential = useSelector(state => state.general.credential)
  return (
    <>
          {/* Authorized Token */}

          <Routes>
            <Route path={"/dashboard"} exact element={<Auth Component = {Layout}/> }>
                <Route path={""} element={<Home />} />
                <Route path={"/dashboard/createbot/:id?"} element={<CreateNewFile />} />
                <Route path={"/dashboard/configuration/:id?"} element={<Configuration />} />
                <Route path={"/dashboard/deploy/:id?"} element={<Deploy />} />
                <Route path={"/dashboard/createtext"} element={<CreateNewFileText />} />
                <Route path={"/dashboard/logs/:id?"} element={<Logs />} />
            </Route>

            {/* UnAuthorized Token */}
{/* 
            <Route path={"/"} exact element={<Login />}/>
            <Route path={"/signup"} element={<Registration />}/>
            <Route path={"/confirm"} element={<Confirmation />}/>
            <Route path={"/forget-password/:token"} element={<ForgetPassword />}/> */}



            <Route path={"/login"} exact element={<Casual Component = {Login}/>}/>
            <Route path={"/signup"} element={<Registration />}/>
            <Route path={"/confirm"} element={<Confirmation />}/>
            <Route path={"/forget-password/:token"} element={<ForgetPassword />}/>
        </Routes>


    </>
  );
}

export default App;
