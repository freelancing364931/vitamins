import { Route, Routes } from "react-router-dom"
import { Confirmation, 
    Login, 
    Registration,
    ForgetPassword,
} from "../../pages"
import { useEffect } from "react"
import { useNavigate } from "react-router-dom"


const Casual = (props) => {
    const history = useNavigate()


    useEffect(() => {
        let token = localStorage.getItem('authToken')
        if(token){
            history("/dashboard")
        }
    })

    const {Component} = props
    return(
        <>
            <Component />
        </>
    )
}

export default Casual