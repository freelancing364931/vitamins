import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

const Auth = (props) => {
    const history =  useNavigate()

    const {Component} = props
    useEffect(() => {

        let token = localStorage.getItem("authToken")
        if(!token){
            history("/login")
        }
    })




    return (
        <>
            <Component />

        </>
    )
}


export default Auth