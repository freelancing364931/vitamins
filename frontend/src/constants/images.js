export default {
    logo: require("../assets/images/logo.svg"),
    user: require("../assets/images/user.svg"),
    menu: require("../assets/images/menu.svg"),
    chat: require("../assets/images/chat_logo.svg"),
    success: require("../assets/images/success.svg"),
    failed: require("../assets/images/fail.svg"),
    warning: require("../assets/images/warning.svg"),
}
