import { useEffect, useState } from "react"
import { Success, Error, Warning } from "../../../constants/svg"

const Toast = ({status, content, show}) => {
    const [alert, setAlert] = useState([
        {
            title: "Success",
            icon: <Success />,
            status: "success"
        },
        {
            title: "Error",
            icon: <Error />,
            status: "error"
        },
        {
            title: "Warning",
            icon: <Warning />,
            status: "warning"
        }]
    )
    const [currentStatus, setCurrentStatus] = useState(null);

    useEffect(() =>{
        setCurrentStatus(alert.find((item) => item.status === status.toLowerCase()));
    }, [alert, status]);

    useEffect(() => {
        if (currentStatus) {
            const timer = setTimeout(() => {
                setCurrentStatus(null);
            }, 3000);
            return () => clearTimeout(timer);
        }
    }, [currentStatus]);

    return(
        <>
            <div className={`toast_wrapper ${status} ${show ? "show" : "hide"}`} >
                <div className="title">
                    {currentStatus?.icon}
                    <h4>{currentStatus?.title}</h4>
                </div>
                <div className="content">
                    {content}
                </div>
            </div>
        </>
    )
}

export default Toast