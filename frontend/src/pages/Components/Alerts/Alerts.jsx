// import { Alert } from "react-bootstrap"
import {setState, useEffect, useState} from "react"


const AlertMessage = ({code, message}) => {
    const [alert, setAlert] = useState(false)

    const [status, setStatus] = useState("")


    useEffect(() => {
        const status = parseInt(code)
        if(status == 400 || status == 401 || status == 403 || status == 404){
            console.log("DANGER")
            setStatus("danger")
        }
        else if(status == 200 || status == 201 || status == 203){
            console.log("SUCCESS")
            setStatus("success")
        }
        console.log(status, message)
    }, [])

    console.log("ALERT MESSAGES")
    return(
        <>
           <div class={`alert alert-${status}`} role="alert">
                {message}
            </div>
        </>
    )
}

export default AlertMessage