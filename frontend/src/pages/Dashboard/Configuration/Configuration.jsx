import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { CurrentTab } from "../../../redux/general/general"
import { useNavigate, useParams } from "react-router-dom"
import { Chat } from "../../Components"
import axios from "axios"
import { baseURL } from "../../../constants/constants"
import AlertMessage from "../../Components/Alerts/Alerts"

const Configuration = () => {

    const {id} = useParams()


    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [data, setData] = useState({bot: "",
                                    title: "",
                                    open_ai_key_api: "",
                                    temperature: "",
                                    model_name: "",
                                    max_token: "",
                                    welcome_message: "",
                                    color: "",
                                    logo: null,
                                    open_ai_key_config: "",
                                    is_active: true
                                    })
    const [alert, setAlert] = useState({ code: "", message: ""})
    const [showAlert, setShowAlert] = useState(false)
    const [disableDeploy, setDisableDeploy] = useState(true)
    const [bot, setBot] = useState()
    const [botData, setBotData] = useState()
    const getData = () => {
        axios.get(`${baseURL}bot/${id}`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }

        })
        .then((res) => {
            // setBot(res.data)
            setDisableDeploy(false)
            setBotData(res.data)
            // console.log(res.data)
        })
        .catch((err) => {
            setShowAlert(true)
            setDisableDeploy(true)
            setAlert({...alert, code: err.response.status, message: err.message})
        })
    }

    const postData = () => {
        axios.post(`${baseURL}configuration/`, {
            title: data.title,
            bot: id,
            open_ai_key_api: data.open_ai_key_api,
            temperature: data.temperature,
            model_name: data.model_name,
            max_token: data.max_token,
            welcome_message: data.welcome,
            color: data.color,
            logo: data.logo,
            open_ai_key_config: data.open_ai_key_config,
            is_active: data.is_active
            

        },{
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }

        })
        .then((res) => {
            setBot(id)
            setAlert({...alert, code: res.response.status, message: res.message});
            setAlert({...alert, code: res.status, message: "Configuration added successfully"})

            setDisableDeploy(false)
            setShowAlert(true)

        })
        .catch((err) => {
                setShowAlert(true)
                setAlert({...alert, code: err.response.status, message: err.message});
        })
    }

    const updateData = () => {
        axios.patch(`${baseURL}bot/${id}`, {
            title: data.title,
            open_ai_key_api: data.open_ai_key_api,
            temperature: data.temperature,
            model_name: data.model_name,
            max_token: data.max_token,
            welcome_message: data.welcome,
            color: data.color,
            logo: data.logo,
            open_ai_key_config: data.open_ai_key_config,
            is_active: data.is_active
            
        },{
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }
        })
        .then((res) => {
            setBot(id)
            setAlert({...alert, code: res.status, message: "Configuration updated successfully"})

            setDisableDeploy(false)
            setShowAlert(true)
        })
        .catch((err) => {
            setShowAlert(true)
            setAlert({...alert, code: err.response.status, message: err.message});
    })
    }

    // useEffect(() => {
    //     console.log(`Alert ===========> ${alert}`)
    //     console.log(`Alert ===========> ${bot}`)
    // }, [alert])

    useEffect(() => {
        dispatch(CurrentTab("configration"))
    }, [])

    useEffect(() => {
        if(id){
            getData()
            setBot(id)
        }
    }, [id])

    useEffect(() => {
        console.log(botData)
        if(botData && botData?.configurations[0]){
            if (botData?.configurations.length > 0){
                setData(
                    {
                        ...data,
                        title: botData?.configurations[0]?.title,
                        open_ai_key_api: botData?.configurations[0]?.open_ai_key_api,
                        temperature: botData?.configurations[0]?.temperature,
                        model_name: botData?.configurations[0]?.model_name,
                        max_token: botData?.configurations[0]?.max_token,
                        welcome_message: botData?.configurations[0]?.welcome_message,
                        color: botData?.configurations[0]?.color,
                        logo: botData?.configurations[0]?.logo,
                        open_ai_key_config: botData?.configurations[0]?.open_ai_key_config,
                        is_active: botData?.configurations[0]?.is_active
                    }
                )
            }
        }
    }, [botData])
    return(
        <>
            <div className="config_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading">
                                <span className="vitamin_title">Bot Ava / Configuration</span>
                                <button className="btn vitamin_btn">Train Now</button>
                            </div>

                            {showAlert && <AlertMessage code = {alert.code} message = {alert.message}/>}
                            
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row">
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                        Bot Name
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.title} onChange={(e) => setData({...data, title: e.target.value})}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <span className="vitamin_section">
                                        OpenAI API
                                    </span>

                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row"> 
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Open Api Key
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.open_ai_key_api} onChange={(e) => setData({...data, open_ai_key_api: e.target.value})}/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Temprature
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.temperature} onChange={(e) => setData({...data, temperature: e.target.value})}/>
                                                </div>

                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Model Name
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.model_name} onChange={(e) =>  setData({...data, model_name: e.target.value})}/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Max Token
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.max_token} onChange={(e) => setData({...data, max_token: e.target.data})}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="row">
                                <div className="col-md-12">
                                    <span className="vitamin_section">
                                        Other Configuration
                                    </span>

                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row"> 
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Welcome Message
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data.welcome_message} onChange={(e) => setData({...data, welcome_message: e.target.value})}/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Color
                                                    </label>
                                                    <input className="form-control vitamin_input"/>
                                                </div>

                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Logo
                                                    </label>
                                                    <input className="form-control vitamin_input"/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    OpenAI api key
                                                    </label>
                                                    <input className="form-control vitamin_input" value={data?.open_ai_key_config} onChange={(e) => setData({...data, open_ai_key_config: e.target.value})}/>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div className="row">
                    <div className="col-lg-6 col-md-12 functional_btn">
                            {bot && botData?.configurations.length > 0 ? (
                                <>
                                <button className="btn vitamin_btn update_btn" onClick={updateData}>Update</button>
                                <button className="btn vitamin_btn deploy_btn"onClick={() => navigate(`/dashboard/deploy/${bot}`)}>Deploy</button>
                                
                                </>

                            ) : (
                                <>
                                <button className="btn vitamin_btn update_btn" onClick={postData}>Save</button>
                                <button className="btn vitamin_btn deploy_btn"onClick={() => navigate(`/dashboard/deploy/${bot}`)} disabled={disableDeploy}>Deploy</button>
                                
                                </>
                            )}
                        </div>
                    </div>
                </div>
                <Chat />

            </div>
        </>
    )
}
export default Configuration