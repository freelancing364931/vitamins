import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { CurrentTab } from "../../../redux/general/general"
import { useNavigate, useParams } from "react-router-dom"
import { Chat } from "../../Components"
import axios from "axios"
import { baseURL } from "../../../constants/constants"
import AlertMessage from "../../Components/Alerts/Alerts"
const Deploy = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const {id} = useParams()



    const [checkedWhatsapp, setCheckedWhatsapp] = useState(false)
    const [checkedDeploy, setCheckedDeploy] = useState(false)
    const [checkedDeployement, setCheckedDeployement] = useState(false)


    const [alert, setAlert] = useState({ code: "", message: ""})
    const [showAlert, setShowAlert] = useState(false)
    const [disableDeploy, setDisableDeploy] = useState(true)
    const [bot, setBot] = useState()
    const [botData, setBotData] = useState()


    const [data, setData] = useState({
        varification_token: "",
        access_token: "",
        phone_number: "",
        web_hook_url: "",
        header: "",
        base_url: "",
        is_active: true,
    })


    const getData = () => {
        axios.get(`${baseURL}bot/${id}`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }

        })
        .then((res) => {
            setDisableDeploy(false)
            setBotData(res.data)
        })
        .catch((err) => {
            setShowAlert(true)
            setDisableDeploy(true)
            setAlert({...alert, code: err.response.status, message: err.message})
        })
    }

    const postData = () => {
        axios.post(`${baseURL}deploy/`, {
            bot: id,
            varification_token: data.varification_token,
            access_token: data.access_token,
            phone_number: data.phone_number,
            web_hook_url: data.web_hook_url,
            header: data.header,
            base_url: data.base_url,
            is_active: data.is_active,
            

        },{
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }

        })
        .then((res) => {
            setBot(id)
            setAlert({...alert, code: res.response.status, message: res.message});
            setAlert({...alert, code: res.status, message: "Deolpy added successfully"})

            setDisableDeploy(false)
            setShowAlert(true)

        })
        .catch((err) => {
                setShowAlert(true)
                setAlert({...alert, code: err.response.status, message: err.message});
        })
    }

    const updateData = () => {
        axios.patch(`${baseURL}deploy/${botData?.deployement[0].id}/`, {
            varification_token: data.varification_token,
            access_token: data.access_token,
            phone_number: data.phone_number,
            web_hook_url: data.web_hook_url,
            header: data.header,
            base_url: data.base_url,
            is_active: data.is_active,
            
        },{
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }
        })
        .then((res) => {
            setBot(id)
            setAlert({...alert, code: res.status, message: "Deploy updated successfully"})

            setDisableDeploy(false)
            setShowAlert(true)
        })
        .catch((err) => {
            setShowAlert(true)
            setAlert({...alert, code: err.response.status, message: err.message});
    })
    }

    useEffect(() => {
        getData()
    }, [id])

    useEffect(() => {
        if(botData && botData.deployement && botData?.deployement?.length > 0) {
            setData({
                ...data,
                varification_token: botData.deployement[0].varification_token,
                access_token: botData.deployement[0].access_token,
                phone_number: botData.deployement[0].phone_number,
                web_hook_url: botData.deployement[0].web_hook_url,
                header: botData.deployement[0].header,
                base_url: botData.deployement[0].base_url,
                is_active: botData.deployement[0].is_active,
            })
        }
    }, [botData])
    return(
        <>
            <div className="deploy_wrapper">
            <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading">
                                <span className="vitamin_title">Bot Ava / Deploy</span>
                            </div>

                            {showAlert && <AlertMessage code = {alert.code} message = {alert.message}/>}
                            
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 vitamin_heading">
                                            <span className="vitamin_section">
                                                Whatsapp Deploy
                                            </span>
                                            <div class="form-check form-switch vitamin_switches">
                                                <input 
                                                    class="form-check-input"
                                                    type="checkbox"
                                                    role="switch"
                                                    id="flexSwitchCheckDefault"
                                                    checked= {checkedWhatsapp}
                                                    onChange={(e) => setCheckedWhatsapp(e.target.checked)}

                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row"> 
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    VARIFICATION_TOKEN
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled = {!checkedWhatsapp} value={data?.varification_token} onChange={((e) => setData({...data, varification_token: e.target.value}))}/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    ACCESS_TOKEN
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled= {!checkedWhatsapp} value={data?.access_token} onChange={((e) => setData({...data, access_token: e.target.value}))}/>
                                                </div>

                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Phone_Number_ID
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled= {!checkedWhatsapp} value={data?.phone_number} onChange={((e) => setData({...data, phone_number: e.target.value}))}/>
                                                </div>
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Web Hook url
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled= {!checkedWhatsapp} value={data?.web_hook_url} onChange={((e) => setData({...data, web_hook_url: e.target.value}))}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 vitamin_heading">
                                            <span className="vitamin_section">
                                                Web Deploy
                                            </span>
                                            <div class="form-check form-switch vitamin_switches">
                                                <input 
                                                class="form-check-input" 
                                                type="checkbox" 
                                                role="switch" 
                                                id="flexSwitchCheckDefault" 
                                                checked= {checkedDeploy}
                                                onChange={(e) => setCheckedDeploy(e.target.checked)}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row"> 
                                                <div className="col-md-12 col-sm-12">
                                                    <label className="label">
                                                    Put this code in header
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled={!checkedDeploy} value={data?.header} onChange={((e) => setData({...data, header: e.target.value}))}/>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 vitamin_heading">
                                            <span className="vitamin_section">
                                                API Deployment
                                            </span>
                                            <div class="form-check form-switch vitamin_switches">
                                                <input 
                                                class="form-check-input" 
                                                type="checkbox" 
                                                role="switch" 
                                                id="flexSwitchCheckDefault" 
                                                checked= {checkedDeployement}
                                                onChange={(e) => setCheckedDeployement(e.target.checked)}
                                                
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row"> 
                                                <div className="col-md-12 col-sm-12">
                                                    <label className="label">
                                                        Base URL
                                                    </label>
                                                    <input className="form-control vitamin_input" disabled={!checkedDeployement} checked={!checkedDeployement} value={data?.base_url} onChange={((e) => setData({...data, base_url: e.target.value}))}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-md-12 functional_btn">
                            {id && botData && botData?.deployement.length > 0 ? (
                                    <>
                                        <button className="btn vitamin_btn update_btn" onClick={updateData}>Update</button>
                                    </>

                                ) : (
                                    <>
                                        <button className="btn vitamin_btn update_btn" onClick={postData}>Save</button>                            
                                    </>
                                )}


                        </div>

                    </div>
                </div>
                <Chat />

            </div>
        </>
    )
}

export default Deploy