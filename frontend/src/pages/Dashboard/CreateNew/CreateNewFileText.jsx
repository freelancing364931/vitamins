import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { CurrentTab } from "../../../redux/general/general"
import { useNavigate } from "react-router-dom"
import { Chat } from "../../Components"
import { useParams } from "react-router-dom"
import { baseURL } from "../../../constants/constants"
import axios from "axios"
import AlertMessage from "../../Components/Alerts/Alerts"
const CreateNewFileText = () => {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const { id } = useParams()


    const [data, setData] = useState({ title: "", description: ""})
    const [alert, setAlert] = useState({ code: "", message: ""})
    const [showAlert, setShowAlert] = useState(false)
    const [disableDeploy, setDisableDeploy] = useState(true)
    const [bot, setBot] = useState()
    const getData = () => {
        axios.get(`${baseURL}bot/${id}`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }

        })
        .then((res) => {
            setBot(res.data)
            setDisableDeploy(false)
        })
        .catch((err) => {
            setShowAlert(true)
            setDisableDeploy(true)
            setAlert({...alert, code: err.response.status, message: err.message})
        })
    }

    const postData = () => {
        axios.post(`${baseURL}bot/`, {
            title: data.title,
            description: data.description,
            

        },{
                headers:{
                    "Authorization": `Bearer ${localStorage.getItem("authToken")}`
                }

        })
        .then((res) => {
            setBot(res.data)
            setDisableDeploy(false)
            setShowAlert(true)

        })
        .catch((err) => {
                setShowAlert(true)
                setAlert({...alert, code: err.response.status, message: err.message});
        })
    }

    const updateData = () => {
        axios.patch(`${baseURL}bot/${id}`, {
            title: data.title,
            description: data.description,
            
        },{
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }
        })
        .then((res) => {
            setBot(res.data)
            setDisableDeploy(false)
            setShowAlert(true)
        })
        .catch((err) => {
            setShowAlert(true)
            setAlert({...alert, code: err.response.status, message: err.message});
    })
    }


    useEffect(() => {
        dispatch(CurrentTab("bot_list"))

        if(id){
            getData()
        }
    }, [])

    useEffect(() => {
        if(showAlert){
            setTimeout(() => {
                setShowAlert(null)
                setAlert(null)
            }, 3000)
        }

    }, [alert])


    return(
        <>
            <div className="create_text_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading">
                                <span className="vitamin_title">Bot Ava / Create/Update</span>
                                <button className="btn vitamin_btn">Train Now</button>
                            </div>

                            {showAlert && <AlertMessage code = {alert.code} message = {alert.message}/>}

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="row">
                                        <div className="col-lg-6 col-md-12 col-sm-12">
                                            <div className="row">
                                                <div className="col-lg-6 col-md-12 col-sm-12">
                                                    <label className="label">
                                                        Bot Name
                                                    </label>
                                                    {/* <input className="form-control vitamin_input" value={data?.title} onChange={(e) => setData({...data, setData(e.target.value)})}/> */}
                                                    <input className="form-control vitamin_input" value={data?.title} onChange={(e) => setData({...data, title: e.target.value})}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* {alert && <AlertMessage code = {show.code} message = {show.message}/>} */}

                        </div>

                        <div className="col-md-12">
                            <label className="label">
                                Content
                            </label>
                            <textarea rows={15} cols={5} className="form-control vitamin_input" value={data?.description} onChange={(e) => setData({...data, description: e.target.value})}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-md-12 functional_btn">
                            {id ? (
                                <>
                                <button className="btn vitamin_btn update_btn" onClick={updateData}>Update</button>
                                <button className="btn vitamin_btn deploy_btn"onClick={() => navigate(`/dashboard/configuration/${id}`)}>Configure</button>
                                
                                </>

                            ) : (
                                <>
                                <button className="btn vitamin_btn update_btn" onClick={postData}>Save</button>
                                <button className="btn vitamin_btn deploy_btn"onClick={() => navigate(`/dashboard/configuration/${bot.id}`)} disabled={disableDeploy}>Configure</button>
                                
                                </>
                            )}
                        </div>
                    </div>
                </div>
                <Chat />

            </div>
        </>
    )
}

export default CreateNewFileText