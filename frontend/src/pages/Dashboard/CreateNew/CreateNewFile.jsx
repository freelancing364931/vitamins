import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import {Link, useNavigate, useParams} from "react-router-dom"
import { CurrentTab } from "../../../redux/general/general"
import { Chat } from "../../Components"
import AlertMessage from "../../Components/Alerts/Alerts"
import axios from "axios"
import { baseURL } from "../../../constants/constants"
const CreateNewFile = () => {

    const navigate = useNavigate()
    const dispatch = useDispatch()


    const [data, setData] = useState([])
    const {id} = useParams()

    useEffect(() => {
        dispatch(CurrentTab("create_new_bot"))
    }, [])

    const getData = () => {
        axios.get(`${baseURL}bot/${id}`,{
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
            }
        })
        .then((res) => setData(res.data))
        .catch((err) => console.log(err))
    }


    useEffect(() => {
        if (id){
            getData()
        }
    }, [id])



    useEffect(() => {

    }, [data])
    return(
        <>
            <div className="create_new_file">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 heading">
                            <span className="vitamin_title">Bot List</span>
                            <button className="vitamin_btn btn" onClick={() => navigate("/dashboard/createtext")}>Deploy</button>
                            <button className="vitamin_btn btn ms-3 me-3" onClick={() => navigate("/dashboard/createtext")}>Configuration</button>
                            <button className="vitamin_btn btn" onClick={() => navigate("/dashboard/createtext")}>Create New File</button>
                        </div>

                        <div className="col-md-12 bot_image_container">
                            <label for="bot_image">
                                <strong>Drag Files</strong>
                                <span>pdf, text, docs</span>
                            </label>
                            <input id="bot_image" className="bot_image" type="file" />
                        </div>

                        <div className="col-md-12 table_container">
                            <table className="table">

                                {data?.files?.length > 0 ? (
                                <>
                                    <tbody>
                                        <tr className="thead">
                                            <th>Title</th>
                                            <th>Files</th>
                                            <th>Train</th>
                                            <th>Configration</th>
                                            <th>Logs</th>
                                        </tr>
                                    {data?.files.map((item, index) => (
                                        <>
                                            <tr>
                                                <td data-label="Title">{item?.file_name}</td>
                                                <td data-label="Files"><a href={`${item?.file}`} target="_blank">file</a></td>
                                                <td data-label="Train">train</td>
                                                <td data-label="Configration"> <a href = {`configuration/${item.id}`} target="_blank" rel="noopener noreferrer">configration</a></td>
                                                <td data-label="Logs">logs</td>
                                            </tr>
                                        </>
                                    ))}


                                        {/* <tr>
                                            <td data-label="Title">File 01</td>
                                            <td data-label="Files">files</td>
                                            <td data-label="Train">train</td>
                                            <td data-label="Configration">configration</td>
                                            <td data-label="Logs">logs</td>
                                        </tr>
                                        <tr>
                                            <td data-label="Title">File 01</td>
                                            <td data-label="Files">files</td>
                                            <td data-label="Train">train</td>
                                            <td data-label="Configration">configration</td>
                                            <td data-label="Logs">logs</td>          
                                        </tr>
                                        <tr>
                                            <td data-label="Title">File 01</td>
                                            <td data-label="Files">files</td>
                                            <td data-label="Train">train</td>
                                            <td data-label="Configration">configration</td>
                                            <td data-label="Logs">logs</td>        
                                        </tr> */}

                                    </tbody>
                                </>

                                ) :
                                (
                                    <>
                                    <tbody>
                                        <tr className="thead">
                                            <th>Title</th>
                                            <th>Files</th>
                                            <th>Train</th>
                                            <th>Configration</th>
                                            <th>Logs</th>
                                        </tr>

                                        <tr r>
                                            <td colSpan={5}>
                                                <AlertMessage code = {200} message = {"There is no files for this Bot"}/>

                                            </td>

                                        </tr>

                                    </tbody>
                                    </>
                                )
                                }
                            </table>
                        </div>
                    </div>
                </div>
                <Chat />
            </div>
        </>
    )
}

export default CreateNewFile