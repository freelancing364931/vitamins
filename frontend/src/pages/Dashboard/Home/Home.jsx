import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import {Link, useNavigate} from "react-router-dom"
import { CurrentTab } from "../../../redux/general/general"
import { Chat } from "../../Components"
import { baseURL } from "../../../constants/constants"
import axios from "axios"
import AlertMessage from "../../Components/Alerts/Alerts"

const Home = () => {

    const navigate = useNavigate()
    const dispatch = useDispatch()

    const [data, setData] = useState()
    const [loader, setLoader] = useState(false)

    const getBots = () =>{
        axios.get(`${baseURL}bot/`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("authToken")}`
              }
        })
        .then((res) => setData(res.data))
        .catch((e) => console.log(e))
    }
    useEffect(() => {
        dispatch(CurrentTab("bot_list"))
        getBots()
        setLoader(true)
    }, [])


    

    return(
        <>
            <div className="home_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 heading">
                            <span className="vitamin_title">Bot List</span>
                            <button className="vitamin_btn btn" onClick={() => navigate("/dashboard/createtext")}>New Bot</button>
                        </div>
                        {data?.length > 0 ? (
                            <>
                                <div className="col-md-12 table_container">
                                    <table className="table">

                                        <tbody>
                                            <tr className="thead">
                                                <th>Title</th>
                                                <th>Files</th>
                                                <th>Train</th>
                                                <th>Configration</th>
                                                <th>Logs</th>
                                            </tr>
                                            {data?.map((item, index) => (
                                                <>                                        
                                                    <tr key={index}>
                                                        <td data-label="Title">{item.title}</td>
                                                        <td data-label="Files"><Link to={`createbot/${item.id}`}>files</Link></td>
                                                        <td data-label="Train"><Link to={`/${item.id}`}>train</Link></td>
                                                        <td data-label="Configration"><Link to={`configuration/${item.id}`}>configration</Link></td>
                                                        <td data-label="Logs"><Link to={`/${item.id}`}>logs</Link></td>
                                                    </tr>
                                                </>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </>
                        )
                            : (
                                <>
                                    <AlertMessage code = {200} message = {"There is Not any Bot Please add a Bot First"}/>
                                </>
                            )
                        }
                    </div>
                </div>
                <Chat />

            </div>
        </>
    )
}

export default Home