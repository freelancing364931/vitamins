import images from "../../constants/images"
import { Link, useNavigate } from "react-router-dom"
import { Credential } from "../../redux/general/general"
import axios from "axios"
import { useEffect, useState } from "react"
import { baseURL } from "../../constants/constants"
// import {AlertMessage} from "../Components/Alerts/Alerts"
import AlertMessage from "../Components/Alerts/Alerts"


const Login = () => {
    const history = useNavigate()

    const [email, setEmail] = useState()
    const [password, setPassword] = useState()


    const [alert, setAlert] = useState(false)
    const [show, setShow] = useState({code: "", message: ""})

    const Loggedin = (e) => {
        e.preventDefault()
        axios.post(baseURL + "auth/login/", {
            email: email,
            password: password
        }).then((res) =>{
            setAlert(true)
            localStorage.setItem("authToken", res.data.access)
            console.log(res.data.access)
            setShow({...show, code: res.status, message: "Logged in successfully"})
            history("/dashboard")

        }).catch((error) => {

            setAlert(true)
            setShow({...show, code: error.response.status, message: error.message})

        })
    }

    useEffect(() => {
        if(alert){
            setTimeout(() => {
                console.log({show})
                setShow({...show, code: "", message:""})
                setAlert(null)
            }, 3000)
        }
    }, [alert])
  


    return(
        <>
            <section className="login_wrapper">
                <div className="container">
                    <div className="row card_row">
                        <div className="col-md-6 shadow offset_md-3 col-lg-4 offset-lg-4 col-sm-12 m-sm-auto bg-white">
                            <div className="login_card text-center">
                                <img src={images.logo.default} alt="Logo" className="logo_img"/>
                                <h1 className="registration_title">Quickbot AI</h1>
                                <h2 className="registration_subtitle">Login</h2>
                                <div className="registration-form">
                                    <div className="row g-3">
                                    {/* <div className="alert alert-danger" role="alert">
                                        A simple danger alert—check it out!
                                    </div> */}
                                    {alert && <AlertMessage code = {show.code} message = {show.message}/>}
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            <label for="email" className="form-label">Email</label>
                                            <input type="email" className="form-control" id="email" placeholder="" value={email} onChange={(e) => setEmail(e.target.value) }/>

                                        </div>
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            <label for="password" className="form-label">Password</label>
                                            <input type="password" className="form-control" id="password" placeholder="" value={password} onChange={(e) => setPassword(e.target.value)}/>
                                        </div>
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            {/* <button className="btn login_btn text-white" onClick={Loggedin}>Login</button> */}
                                            <button className="btn login_btn text-white" onClick={Loggedin}>Login</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}


export default Login