import { useNavigate } from "react-router-dom"
import images from "../../constants/images"
import { useDispatch, useSelector } from "react-redux"
import { Credential } from "../../redux/general/general"
import { useEffect, useState } from "react"
import axios from "axios"
import { baseURL } from "../../constants/constants"
import { useParams } from "react-router-dom";

const ForgetPassword = () => {
    const history = useNavigate()
    const dispatch = useDispatch()
    const {token} = useParams()

    const [data, setData] = useState({
        email: "",
        password: "",
        confirm_password: "",
    })
    const ForgetPasswordApi = (e) => {
        console.log(data)
        e.preventDefault()
        axios.post("auth/forget-password/",{
            email: data.email,
            password: data.password,
            token: token,
            confirm_password: data.confirm_password
        })
        .then((r) => console.log(r))
        .catch((e) => console.log(e))
    }


    const CheckValidationToken = (token) => {
        // console.log(`{TOKEN ===================> ${token}}`)
        axios.post("auth/token-verify/", {
            token: token
        })
        .then((r) => {
            if(r.status == 200){
                console.log(`{Repsonse ${r}}`)
                return true
            }else{
                history("/login")
                return false
            }
        })
        .catch((e) => {
            history("/login")
        })
    }

    useEffect(() => {
        console.log(`{PARAMs==========> ${token}}`)
        CheckValidationToken(token)
    },[])
    return(
        <>
            <section className="register_wrapper">
                <div className="container">
                    <div className="row card_row">
                        <div className="col-md-6 shadow offset_md-3 col-lg-4 offset-lg-4 col-sm-12 m-sm-auto bg-white">
                            <div className="login_card text-center">
                                <img src={images.logo.default} alt="Logo" className="logo_img"/>
                                <h1 className="registration_title">Quickbot AI</h1>
                                <h2 className="registration_subtitle">Reset Password</h2>
                                <div className="registration-form">
                                    <div className="row g-3">
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            <label for="password" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="password" placeholder="" value={data.password} onChange={(e) => setData({...data, password: e.target.value})}/>
                                        </div>
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            <label for="confirm_password" class="form-label">Confirm Password</label>
                                            <input type="password" class="form-control" id="confirm_password" placeholder="" value={data.confirm_password} onChange={(e) => setData({...data, confirm_password: e.target.value})}/>
                                        </div>
                                        <div className="col-sm-10 offset-sm-1 text-start">
                                            <button className="btn login_btn text-white" onClick={ForgetPasswordApi}>Reset Password</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}


export default ForgetPassword